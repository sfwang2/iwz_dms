/**
 * OP_ProvideLinkDataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideLinkDataResponse  implements java.io.Serializable {
    private java.lang.String OP_ProvideLinkDataResult;

    public OP_ProvideLinkDataResponse() {
    }

    public OP_ProvideLinkDataResponse(
           java.lang.String OP_ProvideLinkDataResult) {
           this.OP_ProvideLinkDataResult = OP_ProvideLinkDataResult;
    }


    /**
     * Gets the OP_ProvideLinkDataResult value for this OP_ProvideLinkDataResponse.
     * 
     * @return OP_ProvideLinkDataResult
     */
    public java.lang.String getOP_ProvideLinkDataResult() {
        return OP_ProvideLinkDataResult;
    }


    /**
     * Sets the OP_ProvideLinkDataResult value for this OP_ProvideLinkDataResponse.
     * 
     * @param OP_ProvideLinkDataResult
     */
    public void setOP_ProvideLinkDataResult(java.lang.String OP_ProvideLinkDataResult) {
        this.OP_ProvideLinkDataResult = OP_ProvideLinkDataResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideLinkDataResponse)) return false;
        OP_ProvideLinkDataResponse other = (OP_ProvideLinkDataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideLinkDataResult==null && other.getOP_ProvideLinkDataResult()==null) || 
             (this.OP_ProvideLinkDataResult!=null &&
              this.OP_ProvideLinkDataResult.equals(other.getOP_ProvideLinkDataResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideLinkDataResult() != null) {
            _hashCode += getOP_ProvideLinkDataResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideLinkDataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideLinkDataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideLinkDataResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideLinkDataResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
