/**
 * OP_ProvideEventFullEventUpdate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideEventFullEventUpdate  implements java.io.Serializable {
    private java.lang.String MSG_FullEventUpdateRequest;

    private java.lang.String MSG_EventID;

    private int MSG_UpdateNumber;

    public OP_ProvideEventFullEventUpdate() {
    }

    public OP_ProvideEventFullEventUpdate(
           java.lang.String MSG_FullEventUpdateRequest,
           java.lang.String MSG_EventID,
           int MSG_UpdateNumber) {
           this.MSG_FullEventUpdateRequest = MSG_FullEventUpdateRequest;
           this.MSG_EventID = MSG_EventID;
           this.MSG_UpdateNumber = MSG_UpdateNumber;
    }


    /**
     * Gets the MSG_FullEventUpdateRequest value for this OP_ProvideEventFullEventUpdate.
     * 
     * @return MSG_FullEventUpdateRequest
     */
    public java.lang.String getMSG_FullEventUpdateRequest() {
        return MSG_FullEventUpdateRequest;
    }


    /**
     * Sets the MSG_FullEventUpdateRequest value for this OP_ProvideEventFullEventUpdate.
     * 
     * @param MSG_FullEventUpdateRequest
     */
    public void setMSG_FullEventUpdateRequest(java.lang.String MSG_FullEventUpdateRequest) {
        this.MSG_FullEventUpdateRequest = MSG_FullEventUpdateRequest;
    }


    /**
     * Gets the MSG_EventID value for this OP_ProvideEventFullEventUpdate.
     * 
     * @return MSG_EventID
     */
    public java.lang.String getMSG_EventID() {
        return MSG_EventID;
    }


    /**
     * Sets the MSG_EventID value for this OP_ProvideEventFullEventUpdate.
     * 
     * @param MSG_EventID
     */
    public void setMSG_EventID(java.lang.String MSG_EventID) {
        this.MSG_EventID = MSG_EventID;
    }


    /**
     * Gets the MSG_UpdateNumber value for this OP_ProvideEventFullEventUpdate.
     * 
     * @return MSG_UpdateNumber
     */
    public int getMSG_UpdateNumber() {
        return MSG_UpdateNumber;
    }


    /**
     * Sets the MSG_UpdateNumber value for this OP_ProvideEventFullEventUpdate.
     * 
     * @param MSG_UpdateNumber
     */
    public void setMSG_UpdateNumber(int MSG_UpdateNumber) {
        this.MSG_UpdateNumber = MSG_UpdateNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideEventFullEventUpdate)) return false;
        OP_ProvideEventFullEventUpdate other = (OP_ProvideEventFullEventUpdate) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_FullEventUpdateRequest==null && other.getMSG_FullEventUpdateRequest()==null) || 
             (this.MSG_FullEventUpdateRequest!=null &&
              this.MSG_FullEventUpdateRequest.equals(other.getMSG_FullEventUpdateRequest()))) &&
            ((this.MSG_EventID==null && other.getMSG_EventID()==null) || 
             (this.MSG_EventID!=null &&
              this.MSG_EventID.equals(other.getMSG_EventID()))) &&
            this.MSG_UpdateNumber == other.getMSG_UpdateNumber();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_FullEventUpdateRequest() != null) {
            _hashCode += getMSG_FullEventUpdateRequest().hashCode();
        }
        if (getMSG_EventID() != null) {
            _hashCode += getMSG_EventID().hashCode();
        }
        _hashCode += getMSG_UpdateNumber();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideEventFullEventUpdate.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideEventFullEventUpdate"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_FullEventUpdateRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_FullEventUpdateRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_EventID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_EventID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_UpdateNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_UpdateNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
