/**
 * TmddV2Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class TmddV2Response  implements java.io.Serializable {
    private java.lang.String tmddV2Result;

    public TmddV2Response() {
    }

    public TmddV2Response(
           java.lang.String tmddV2Result) {
           this.tmddV2Result = tmddV2Result;
    }


    /**
     * Gets the tmddV2Result value for this TmddV2Response.
     * 
     * @return tmddV2Result
     */
    public java.lang.String getTmddV2Result() {
        return tmddV2Result;
    }


    /**
     * Sets the tmddV2Result value for this TmddV2Response.
     * 
     * @param tmddV2Result
     */
    public void setTmddV2Result(java.lang.String tmddV2Result) {
        this.tmddV2Result = tmddV2Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TmddV2Response)) return false;
        TmddV2Response other = (TmddV2Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tmddV2Result==null && other.getTmddV2Result()==null) || 
             (this.tmddV2Result!=null &&
              this.tmddV2Result.equals(other.getTmddV2Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTmddV2Result() != null) {
            _hashCode += getTmddV2Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TmddV2Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">TmddV2Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tmddV2Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "TmddV2Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
