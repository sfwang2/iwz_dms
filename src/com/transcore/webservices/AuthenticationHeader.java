/**
 * AuthenticationHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class AuthenticationHeader  implements java.io.Serializable {
    private byte[] domain;

    private byte[] username;

    private byte[] password;

    public AuthenticationHeader() {
    }

    public AuthenticationHeader(
           byte[] domain,
           byte[] username,
           byte[] password) {
           this.domain = domain;
           this.username = username;
           this.password = password;
    }


    /**
     * Gets the domain value for this AuthenticationHeader.
     * 
     * @return domain
     */
    public byte[] getDomain() {
        return domain;
    }


    /**
     * Sets the domain value for this AuthenticationHeader.
     * 
     * @param domain
     */
    public void setDomain(byte[] domain) {
        this.domain = domain;
    }


    /**
     * Gets the username value for this AuthenticationHeader.
     * 
     * @return username
     */
    public byte[] getUsername() {
        return username;
    }


    /**
     * Sets the username value for this AuthenticationHeader.
     * 
     * @param username
     */
    public void setUsername(byte[] username) {
        this.username = username;
    }


    /**
     * Gets the password value for this AuthenticationHeader.
     * 
     * @return password
     */
    public byte[] getPassword() {
        return password;
    }


    /**
     * Sets the password value for this AuthenticationHeader.
     * 
     * @param password
     */
    public void setPassword(byte[] password) {
        this.password = password;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthenticationHeader)) return false;
        AuthenticationHeader other = (AuthenticationHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.domain==null && other.getDomain()==null) || 
             (this.domain!=null &&
              java.util.Arrays.equals(this.domain, other.getDomain()))) &&
            ((this.username==null && other.getUsername()==null) || 
             (this.username!=null &&
              java.util.Arrays.equals(this.username, other.getUsername()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              java.util.Arrays.equals(this.password, other.getPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDomain() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDomain());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDomain(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUsername() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUsername());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUsername(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassword() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassword());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassword(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthenticationHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", "AuthenticationHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("domain");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "Domain"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("username");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "Username"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "Password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
