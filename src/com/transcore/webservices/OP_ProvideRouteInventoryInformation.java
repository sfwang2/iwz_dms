/**
 * OP_ProvideRouteInventoryInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideRouteInventoryInformation  implements java.io.Serializable {
    private java.lang.String MSG_RouteInventoryRequest;

    public OP_ProvideRouteInventoryInformation() {
    }

    public OP_ProvideRouteInventoryInformation(
           java.lang.String MSG_RouteInventoryRequest) {
           this.MSG_RouteInventoryRequest = MSG_RouteInventoryRequest;
    }


    /**
     * Gets the MSG_RouteInventoryRequest value for this OP_ProvideRouteInventoryInformation.
     * 
     * @return MSG_RouteInventoryRequest
     */
    public java.lang.String getMSG_RouteInventoryRequest() {
        return MSG_RouteInventoryRequest;
    }


    /**
     * Sets the MSG_RouteInventoryRequest value for this OP_ProvideRouteInventoryInformation.
     * 
     * @param MSG_RouteInventoryRequest
     */
    public void setMSG_RouteInventoryRequest(java.lang.String MSG_RouteInventoryRequest) {
        this.MSG_RouteInventoryRequest = MSG_RouteInventoryRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideRouteInventoryInformation)) return false;
        OP_ProvideRouteInventoryInformation other = (OP_ProvideRouteInventoryInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_RouteInventoryRequest==null && other.getMSG_RouteInventoryRequest()==null) || 
             (this.MSG_RouteInventoryRequest!=null &&
              this.MSG_RouteInventoryRequest.equals(other.getMSG_RouteInventoryRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_RouteInventoryRequest() != null) {
            _hashCode += getMSG_RouteInventoryRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideRouteInventoryInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideRouteInventoryInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_RouteInventoryRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_RouteInventoryRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
