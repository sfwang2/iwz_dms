/**
 * OP_ShareRampMeterInventoryInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareRampMeterInventoryInformation  implements java.io.Serializable {
    private java.lang.String MSG_RampMeterInventoryRequest;

    public OP_ShareRampMeterInventoryInformation() {
    }

    public OP_ShareRampMeterInventoryInformation(
           java.lang.String MSG_RampMeterInventoryRequest) {
           this.MSG_RampMeterInventoryRequest = MSG_RampMeterInventoryRequest;
    }


    /**
     * Gets the MSG_RampMeterInventoryRequest value for this OP_ShareRampMeterInventoryInformation.
     * 
     * @return MSG_RampMeterInventoryRequest
     */
    public java.lang.String getMSG_RampMeterInventoryRequest() {
        return MSG_RampMeterInventoryRequest;
    }


    /**
     * Sets the MSG_RampMeterInventoryRequest value for this OP_ShareRampMeterInventoryInformation.
     * 
     * @param MSG_RampMeterInventoryRequest
     */
    public void setMSG_RampMeterInventoryRequest(java.lang.String MSG_RampMeterInventoryRequest) {
        this.MSG_RampMeterInventoryRequest = MSG_RampMeterInventoryRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareRampMeterInventoryInformation)) return false;
        OP_ShareRampMeterInventoryInformation other = (OP_ShareRampMeterInventoryInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_RampMeterInventoryRequest==null && other.getMSG_RampMeterInventoryRequest()==null) || 
             (this.MSG_RampMeterInventoryRequest!=null &&
              this.MSG_RampMeterInventoryRequest.equals(other.getMSG_RampMeterInventoryRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_RampMeterInventoryRequest() != null) {
            _hashCode += getMSG_RampMeterInventoryRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareRampMeterInventoryInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareRampMeterInventoryInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_RampMeterInventoryRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_RampMeterInventoryRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
