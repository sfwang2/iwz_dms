/**
 * OP_ProvideEventFullEventUpdateResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideEventFullEventUpdateResponse  implements java.io.Serializable {
    private com.transcore.webservices.OP_ProvideEventFullEventUpdateResponseOP_ProvideEventFullEventUpdateResult OP_ProvideEventFullEventUpdateResult;

    public OP_ProvideEventFullEventUpdateResponse() {
    }

    public OP_ProvideEventFullEventUpdateResponse(
           com.transcore.webservices.OP_ProvideEventFullEventUpdateResponseOP_ProvideEventFullEventUpdateResult OP_ProvideEventFullEventUpdateResult) {
           this.OP_ProvideEventFullEventUpdateResult = OP_ProvideEventFullEventUpdateResult;
    }


    /**
     * Gets the OP_ProvideEventFullEventUpdateResult value for this OP_ProvideEventFullEventUpdateResponse.
     * 
     * @return OP_ProvideEventFullEventUpdateResult
     */
    public com.transcore.webservices.OP_ProvideEventFullEventUpdateResponseOP_ProvideEventFullEventUpdateResult getOP_ProvideEventFullEventUpdateResult() {
        return OP_ProvideEventFullEventUpdateResult;
    }


    /**
     * Sets the OP_ProvideEventFullEventUpdateResult value for this OP_ProvideEventFullEventUpdateResponse.
     * 
     * @param OP_ProvideEventFullEventUpdateResult
     */
    public void setOP_ProvideEventFullEventUpdateResult(com.transcore.webservices.OP_ProvideEventFullEventUpdateResponseOP_ProvideEventFullEventUpdateResult OP_ProvideEventFullEventUpdateResult) {
        this.OP_ProvideEventFullEventUpdateResult = OP_ProvideEventFullEventUpdateResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideEventFullEventUpdateResponse)) return false;
        OP_ProvideEventFullEventUpdateResponse other = (OP_ProvideEventFullEventUpdateResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideEventFullEventUpdateResult==null && other.getOP_ProvideEventFullEventUpdateResult()==null) || 
             (this.OP_ProvideEventFullEventUpdateResult!=null &&
              this.OP_ProvideEventFullEventUpdateResult.equals(other.getOP_ProvideEventFullEventUpdateResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideEventFullEventUpdateResult() != null) {
            _hashCode += getOP_ProvideEventFullEventUpdateResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideEventFullEventUpdateResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideEventFullEventUpdateResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideEventFullEventUpdateResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideEventFullEventUpdateResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">>OP_ProvideEventFullEventUpdateResponse>OP_ProvideEventFullEventUpdateResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
