/**
 * OP_ProvideTrafficNetworkInventoryInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideTrafficNetworkInventoryInformation  implements java.io.Serializable {
    private java.lang.String MSG_TrafficNetworkRequest;

    public OP_ProvideTrafficNetworkInventoryInformation() {
    }

    public OP_ProvideTrafficNetworkInventoryInformation(
           java.lang.String MSG_TrafficNetworkRequest) {
           this.MSG_TrafficNetworkRequest = MSG_TrafficNetworkRequest;
    }


    /**
     * Gets the MSG_TrafficNetworkRequest value for this OP_ProvideTrafficNetworkInventoryInformation.
     * 
     * @return MSG_TrafficNetworkRequest
     */
    public java.lang.String getMSG_TrafficNetworkRequest() {
        return MSG_TrafficNetworkRequest;
    }


    /**
     * Sets the MSG_TrafficNetworkRequest value for this OP_ProvideTrafficNetworkInventoryInformation.
     * 
     * @param MSG_TrafficNetworkRequest
     */
    public void setMSG_TrafficNetworkRequest(java.lang.String MSG_TrafficNetworkRequest) {
        this.MSG_TrafficNetworkRequest = MSG_TrafficNetworkRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideTrafficNetworkInventoryInformation)) return false;
        OP_ProvideTrafficNetworkInventoryInformation other = (OP_ProvideTrafficNetworkInventoryInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_TrafficNetworkRequest==null && other.getMSG_TrafficNetworkRequest()==null) || 
             (this.MSG_TrafficNetworkRequest!=null &&
              this.MSG_TrafficNetworkRequest.equals(other.getMSG_TrafficNetworkRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_TrafficNetworkRequest() != null) {
            _hashCode += getMSG_TrafficNetworkRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideTrafficNetworkInventoryInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideTrafficNetworkInventoryInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_TrafficNetworkRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_TrafficNetworkRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
