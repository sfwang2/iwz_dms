/**
 * OP_ProvideNodeStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideNodeStatusResponse  implements java.io.Serializable {
    private java.lang.String OP_ProvideNodeStatusResult;

    public OP_ProvideNodeStatusResponse() {
    }

    public OP_ProvideNodeStatusResponse(
           java.lang.String OP_ProvideNodeStatusResult) {
           this.OP_ProvideNodeStatusResult = OP_ProvideNodeStatusResult;
    }


    /**
     * Gets the OP_ProvideNodeStatusResult value for this OP_ProvideNodeStatusResponse.
     * 
     * @return OP_ProvideNodeStatusResult
     */
    public java.lang.String getOP_ProvideNodeStatusResult() {
        return OP_ProvideNodeStatusResult;
    }


    /**
     * Sets the OP_ProvideNodeStatusResult value for this OP_ProvideNodeStatusResponse.
     * 
     * @param OP_ProvideNodeStatusResult
     */
    public void setOP_ProvideNodeStatusResult(java.lang.String OP_ProvideNodeStatusResult) {
        this.OP_ProvideNodeStatusResult = OP_ProvideNodeStatusResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideNodeStatusResponse)) return false;
        OP_ProvideNodeStatusResponse other = (OP_ProvideNodeStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideNodeStatusResult==null && other.getOP_ProvideNodeStatusResult()==null) || 
             (this.OP_ProvideNodeStatusResult!=null &&
              this.OP_ProvideNodeStatusResult.equals(other.getOP_ProvideNodeStatusResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideNodeStatusResult() != null) {
            _hashCode += getOP_ProvideNodeStatusResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideNodeStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideNodeStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideNodeStatusResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideNodeStatusResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
