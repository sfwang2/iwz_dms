/**
 * SVC_C2C.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public interface SVC_C2C extends javax.xml.rpc.Service {
    public java.lang.String getSVC_C2CSoapAddress();

    public com.transcore.webservices.SVC_C2CSoap getSVC_C2CSoap() throws javax.xml.rpc.ServiceException;

    public com.transcore.webservices.SVC_C2CSoap getSVC_C2CSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
