/**
 * OP_ShareTrafficDetectorInventoryInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareTrafficDetectorInventoryInformation  implements java.io.Serializable {
    private java.lang.String MSG_TrafficDetectorInventoryRequest;

    public OP_ShareTrafficDetectorInventoryInformation() {
    }

    public OP_ShareTrafficDetectorInventoryInformation(
           java.lang.String MSG_TrafficDetectorInventoryRequest) {
           this.MSG_TrafficDetectorInventoryRequest = MSG_TrafficDetectorInventoryRequest;
    }


    /**
     * Gets the MSG_TrafficDetectorInventoryRequest value for this OP_ShareTrafficDetectorInventoryInformation.
     * 
     * @return MSG_TrafficDetectorInventoryRequest
     */
    public java.lang.String getMSG_TrafficDetectorInventoryRequest() {
        return MSG_TrafficDetectorInventoryRequest;
    }


    /**
     * Sets the MSG_TrafficDetectorInventoryRequest value for this OP_ShareTrafficDetectorInventoryInformation.
     * 
     * @param MSG_TrafficDetectorInventoryRequest
     */
    public void setMSG_TrafficDetectorInventoryRequest(java.lang.String MSG_TrafficDetectorInventoryRequest) {
        this.MSG_TrafficDetectorInventoryRequest = MSG_TrafficDetectorInventoryRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareTrafficDetectorInventoryInformation)) return false;
        OP_ShareTrafficDetectorInventoryInformation other = (OP_ShareTrafficDetectorInventoryInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_TrafficDetectorInventoryRequest==null && other.getMSG_TrafficDetectorInventoryRequest()==null) || 
             (this.MSG_TrafficDetectorInventoryRequest!=null &&
              this.MSG_TrafficDetectorInventoryRequest.equals(other.getMSG_TrafficDetectorInventoryRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_TrafficDetectorInventoryRequest() != null) {
            _hashCode += getMSG_TrafficDetectorInventoryRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareTrafficDetectorInventoryInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareTrafficDetectorInventoryInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_TrafficDetectorInventoryRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_TrafficDetectorInventoryRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
