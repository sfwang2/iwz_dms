/**
 * OP_ShareRampMeterInventoryInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareRampMeterInventoryInformationResponse  implements java.io.Serializable {
    private java.lang.String OP_ShareRampMeterInventoryInformationResult;

    public OP_ShareRampMeterInventoryInformationResponse() {
    }

    public OP_ShareRampMeterInventoryInformationResponse(
           java.lang.String OP_ShareRampMeterInventoryInformationResult) {
           this.OP_ShareRampMeterInventoryInformationResult = OP_ShareRampMeterInventoryInformationResult;
    }


    /**
     * Gets the OP_ShareRampMeterInventoryInformationResult value for this OP_ShareRampMeterInventoryInformationResponse.
     * 
     * @return OP_ShareRampMeterInventoryInformationResult
     */
    public java.lang.String getOP_ShareRampMeterInventoryInformationResult() {
        return OP_ShareRampMeterInventoryInformationResult;
    }


    /**
     * Sets the OP_ShareRampMeterInventoryInformationResult value for this OP_ShareRampMeterInventoryInformationResponse.
     * 
     * @param OP_ShareRampMeterInventoryInformationResult
     */
    public void setOP_ShareRampMeterInventoryInformationResult(java.lang.String OP_ShareRampMeterInventoryInformationResult) {
        this.OP_ShareRampMeterInventoryInformationResult = OP_ShareRampMeterInventoryInformationResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareRampMeterInventoryInformationResponse)) return false;
        OP_ShareRampMeterInventoryInformationResponse other = (OP_ShareRampMeterInventoryInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ShareRampMeterInventoryInformationResult==null && other.getOP_ShareRampMeterInventoryInformationResult()==null) || 
             (this.OP_ShareRampMeterInventoryInformationResult!=null &&
              this.OP_ShareRampMeterInventoryInformationResult.equals(other.getOP_ShareRampMeterInventoryInformationResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ShareRampMeterInventoryInformationResult() != null) {
            _hashCode += getOP_ShareRampMeterInventoryInformationResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareRampMeterInventoryInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareRampMeterInventoryInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ShareRampMeterInventoryInformationResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ShareRampMeterInventoryInformationResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
