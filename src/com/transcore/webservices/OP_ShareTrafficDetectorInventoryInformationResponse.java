/**
 * OP_ShareTrafficDetectorInventoryInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareTrafficDetectorInventoryInformationResponse  implements java.io.Serializable {
    private java.lang.String OP_ShareTrafficDetectorInventoryInformationResult;

    public OP_ShareTrafficDetectorInventoryInformationResponse() {
    }

    public OP_ShareTrafficDetectorInventoryInformationResponse(
           java.lang.String OP_ShareTrafficDetectorInventoryInformationResult) {
           this.OP_ShareTrafficDetectorInventoryInformationResult = OP_ShareTrafficDetectorInventoryInformationResult;
    }


    /**
     * Gets the OP_ShareTrafficDetectorInventoryInformationResult value for this OP_ShareTrafficDetectorInventoryInformationResponse.
     * 
     * @return OP_ShareTrafficDetectorInventoryInformationResult
     */
    public java.lang.String getOP_ShareTrafficDetectorInventoryInformationResult() {
        return OP_ShareTrafficDetectorInventoryInformationResult;
    }


    /**
     * Sets the OP_ShareTrafficDetectorInventoryInformationResult value for this OP_ShareTrafficDetectorInventoryInformationResponse.
     * 
     * @param OP_ShareTrafficDetectorInventoryInformationResult
     */
    public void setOP_ShareTrafficDetectorInventoryInformationResult(java.lang.String OP_ShareTrafficDetectorInventoryInformationResult) {
        this.OP_ShareTrafficDetectorInventoryInformationResult = OP_ShareTrafficDetectorInventoryInformationResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareTrafficDetectorInventoryInformationResponse)) return false;
        OP_ShareTrafficDetectorInventoryInformationResponse other = (OP_ShareTrafficDetectorInventoryInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ShareTrafficDetectorInventoryInformationResult==null && other.getOP_ShareTrafficDetectorInventoryInformationResult()==null) || 
             (this.OP_ShareTrafficDetectorInventoryInformationResult!=null &&
              this.OP_ShareTrafficDetectorInventoryInformationResult.equals(other.getOP_ShareTrafficDetectorInventoryInformationResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ShareTrafficDetectorInventoryInformationResult() != null) {
            _hashCode += getOP_ShareTrafficDetectorInventoryInformationResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareTrafficDetectorInventoryInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareTrafficDetectorInventoryInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ShareTrafficDetectorInventoryInformationResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ShareTrafficDetectorInventoryInformationResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
