/**
 * OP_ShareCCTVInventoryInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareCCTVInventoryInformation  implements java.io.Serializable {
    private java.lang.String MSG_CCTVInventoryRequest;

    public OP_ShareCCTVInventoryInformation() {
    }

    public OP_ShareCCTVInventoryInformation(
           java.lang.String MSG_CCTVInventoryRequest) {
           this.MSG_CCTVInventoryRequest = MSG_CCTVInventoryRequest;
    }


    /**
     * Gets the MSG_CCTVInventoryRequest value for this OP_ShareCCTVInventoryInformation.
     * 
     * @return MSG_CCTVInventoryRequest
     */
    public java.lang.String getMSG_CCTVInventoryRequest() {
        return MSG_CCTVInventoryRequest;
    }


    /**
     * Sets the MSG_CCTVInventoryRequest value for this OP_ShareCCTVInventoryInformation.
     * 
     * @param MSG_CCTVInventoryRequest
     */
    public void setMSG_CCTVInventoryRequest(java.lang.String MSG_CCTVInventoryRequest) {
        this.MSG_CCTVInventoryRequest = MSG_CCTVInventoryRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareCCTVInventoryInformation)) return false;
        OP_ShareCCTVInventoryInformation other = (OP_ShareCCTVInventoryInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_CCTVInventoryRequest==null && other.getMSG_CCTVInventoryRequest()==null) || 
             (this.MSG_CCTVInventoryRequest!=null &&
              this.MSG_CCTVInventoryRequest.equals(other.getMSG_CCTVInventoryRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_CCTVInventoryRequest() != null) {
            _hashCode += getMSG_CCTVInventoryRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareCCTVInventoryInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareCCTVInventoryInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_CCTVInventoryRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_CCTVInventoryRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
