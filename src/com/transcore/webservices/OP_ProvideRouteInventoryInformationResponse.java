/**
 * OP_ProvideRouteInventoryInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideRouteInventoryInformationResponse  implements java.io.Serializable {
    private java.lang.String OP_ProvideRouteInventoryInformationResult;

    public OP_ProvideRouteInventoryInformationResponse() {
    }

    public OP_ProvideRouteInventoryInformationResponse(
           java.lang.String OP_ProvideRouteInventoryInformationResult) {
           this.OP_ProvideRouteInventoryInformationResult = OP_ProvideRouteInventoryInformationResult;
    }


    /**
     * Gets the OP_ProvideRouteInventoryInformationResult value for this OP_ProvideRouteInventoryInformationResponse.
     * 
     * @return OP_ProvideRouteInventoryInformationResult
     */
    public java.lang.String getOP_ProvideRouteInventoryInformationResult() {
        return OP_ProvideRouteInventoryInformationResult;
    }


    /**
     * Sets the OP_ProvideRouteInventoryInformationResult value for this OP_ProvideRouteInventoryInformationResponse.
     * 
     * @param OP_ProvideRouteInventoryInformationResult
     */
    public void setOP_ProvideRouteInventoryInformationResult(java.lang.String OP_ProvideRouteInventoryInformationResult) {
        this.OP_ProvideRouteInventoryInformationResult = OP_ProvideRouteInventoryInformationResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideRouteInventoryInformationResponse)) return false;
        OP_ProvideRouteInventoryInformationResponse other = (OP_ProvideRouteInventoryInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideRouteInventoryInformationResult==null && other.getOP_ProvideRouteInventoryInformationResult()==null) || 
             (this.OP_ProvideRouteInventoryInformationResult!=null &&
              this.OP_ProvideRouteInventoryInformationResult.equals(other.getOP_ProvideRouteInventoryInformationResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideRouteInventoryInformationResult() != null) {
            _hashCode += getOP_ProvideRouteInventoryInformationResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideRouteInventoryInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideRouteInventoryInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideRouteInventoryInformationResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideRouteInventoryInformationResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
