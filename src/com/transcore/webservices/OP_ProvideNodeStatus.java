/**
 * OP_ProvideNodeStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideNodeStatus  implements java.io.Serializable {
    private java.lang.String MSG_NodeStatusRequest;

    public OP_ProvideNodeStatus() {
    }

    public OP_ProvideNodeStatus(
           java.lang.String MSG_NodeStatusRequest) {
           this.MSG_NodeStatusRequest = MSG_NodeStatusRequest;
    }


    /**
     * Gets the MSG_NodeStatusRequest value for this OP_ProvideNodeStatus.
     * 
     * @return MSG_NodeStatusRequest
     */
    public java.lang.String getMSG_NodeStatusRequest() {
        return MSG_NodeStatusRequest;
    }


    /**
     * Sets the MSG_NodeStatusRequest value for this OP_ProvideNodeStatus.
     * 
     * @param MSG_NodeStatusRequest
     */
    public void setMSG_NodeStatusRequest(java.lang.String MSG_NodeStatusRequest) {
        this.MSG_NodeStatusRequest = MSG_NodeStatusRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideNodeStatus)) return false;
        OP_ProvideNodeStatus other = (OP_ProvideNodeStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_NodeStatusRequest==null && other.getMSG_NodeStatusRequest()==null) || 
             (this.MSG_NodeStatusRequest!=null &&
              this.MSG_NodeStatusRequest.equals(other.getMSG_NodeStatusRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_NodeStatusRequest() != null) {
            _hashCode += getMSG_NodeStatusRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideNodeStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideNodeStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_NodeStatusRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_NodeStatusRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
