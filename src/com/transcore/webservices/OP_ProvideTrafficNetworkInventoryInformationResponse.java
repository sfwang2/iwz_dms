/**
 * OP_ProvideTrafficNetworkInventoryInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideTrafficNetworkInventoryInformationResponse  implements java.io.Serializable {
    private java.lang.String OP_ProvideTrafficNetworkInventoryInformationResult;

    public OP_ProvideTrafficNetworkInventoryInformationResponse() {
    }

    public OP_ProvideTrafficNetworkInventoryInformationResponse(
           java.lang.String OP_ProvideTrafficNetworkInventoryInformationResult) {
           this.OP_ProvideTrafficNetworkInventoryInformationResult = OP_ProvideTrafficNetworkInventoryInformationResult;
    }


    /**
     * Gets the OP_ProvideTrafficNetworkInventoryInformationResult value for this OP_ProvideTrafficNetworkInventoryInformationResponse.
     * 
     * @return OP_ProvideTrafficNetworkInventoryInformationResult
     */
    public java.lang.String getOP_ProvideTrafficNetworkInventoryInformationResult() {
        return OP_ProvideTrafficNetworkInventoryInformationResult;
    }


    /**
     * Sets the OP_ProvideTrafficNetworkInventoryInformationResult value for this OP_ProvideTrafficNetworkInventoryInformationResponse.
     * 
     * @param OP_ProvideTrafficNetworkInventoryInformationResult
     */
    public void setOP_ProvideTrafficNetworkInventoryInformationResult(java.lang.String OP_ProvideTrafficNetworkInventoryInformationResult) {
        this.OP_ProvideTrafficNetworkInventoryInformationResult = OP_ProvideTrafficNetworkInventoryInformationResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideTrafficNetworkInventoryInformationResponse)) return false;
        OP_ProvideTrafficNetworkInventoryInformationResponse other = (OP_ProvideTrafficNetworkInventoryInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideTrafficNetworkInventoryInformationResult==null && other.getOP_ProvideTrafficNetworkInventoryInformationResult()==null) || 
             (this.OP_ProvideTrafficNetworkInventoryInformationResult!=null &&
              this.OP_ProvideTrafficNetworkInventoryInformationResult.equals(other.getOP_ProvideTrafficNetworkInventoryInformationResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideTrafficNetworkInventoryInformationResult() != null) {
            _hashCode += getOP_ProvideTrafficNetworkInventoryInformationResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideTrafficNetworkInventoryInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideTrafficNetworkInventoryInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideTrafficNetworkInventoryInformationResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideTrafficNetworkInventoryInformationResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
