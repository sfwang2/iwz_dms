/**
 * OP_ProvideEventIndexMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideEventIndexMessage  implements java.io.Serializable {
    private java.lang.String MSG_EventIndexMessageRequest;

    public OP_ProvideEventIndexMessage() {
    }

    public OP_ProvideEventIndexMessage(
           java.lang.String MSG_EventIndexMessageRequest) {
           this.MSG_EventIndexMessageRequest = MSG_EventIndexMessageRequest;
    }


    /**
     * Gets the MSG_EventIndexMessageRequest value for this OP_ProvideEventIndexMessage.
     * 
     * @return MSG_EventIndexMessageRequest
     */
    public java.lang.String getMSG_EventIndexMessageRequest() {
        return MSG_EventIndexMessageRequest;
    }


    /**
     * Sets the MSG_EventIndexMessageRequest value for this OP_ProvideEventIndexMessage.
     * 
     * @param MSG_EventIndexMessageRequest
     */
    public void setMSG_EventIndexMessageRequest(java.lang.String MSG_EventIndexMessageRequest) {
        this.MSG_EventIndexMessageRequest = MSG_EventIndexMessageRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideEventIndexMessage)) return false;
        OP_ProvideEventIndexMessage other = (OP_ProvideEventIndexMessage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_EventIndexMessageRequest==null && other.getMSG_EventIndexMessageRequest()==null) || 
             (this.MSG_EventIndexMessageRequest!=null &&
              this.MSG_EventIndexMessageRequest.equals(other.getMSG_EventIndexMessageRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_EventIndexMessageRequest() != null) {
            _hashCode += getMSG_EventIndexMessageRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideEventIndexMessage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideEventIndexMessage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_EventIndexMessageRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_EventIndexMessageRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
