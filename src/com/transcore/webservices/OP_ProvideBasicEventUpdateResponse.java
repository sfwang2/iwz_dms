/**
 * OP_ProvideBasicEventUpdateResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideBasicEventUpdateResponse  implements java.io.Serializable {
    private java.lang.String OP_ProvideBasicEventUpdateResult;

    public OP_ProvideBasicEventUpdateResponse() {
    }

    public OP_ProvideBasicEventUpdateResponse(
           java.lang.String OP_ProvideBasicEventUpdateResult) {
           this.OP_ProvideBasicEventUpdateResult = OP_ProvideBasicEventUpdateResult;
    }


    /**
     * Gets the OP_ProvideBasicEventUpdateResult value for this OP_ProvideBasicEventUpdateResponse.
     * 
     * @return OP_ProvideBasicEventUpdateResult
     */
    public java.lang.String getOP_ProvideBasicEventUpdateResult() {
        return OP_ProvideBasicEventUpdateResult;
    }


    /**
     * Sets the OP_ProvideBasicEventUpdateResult value for this OP_ProvideBasicEventUpdateResponse.
     * 
     * @param OP_ProvideBasicEventUpdateResult
     */
    public void setOP_ProvideBasicEventUpdateResult(java.lang.String OP_ProvideBasicEventUpdateResult) {
        this.OP_ProvideBasicEventUpdateResult = OP_ProvideBasicEventUpdateResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideBasicEventUpdateResponse)) return false;
        OP_ProvideBasicEventUpdateResponse other = (OP_ProvideBasicEventUpdateResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideBasicEventUpdateResult==null && other.getOP_ProvideBasicEventUpdateResult()==null) || 
             (this.OP_ProvideBasicEventUpdateResult!=null &&
              this.OP_ProvideBasicEventUpdateResult.equals(other.getOP_ProvideBasicEventUpdateResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideBasicEventUpdateResult() != null) {
            _hashCode += getOP_ProvideBasicEventUpdateResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideBasicEventUpdateResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideBasicEventUpdateResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideBasicEventUpdateResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideBasicEventUpdateResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
