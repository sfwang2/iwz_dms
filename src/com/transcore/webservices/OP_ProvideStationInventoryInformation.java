/**
 * OP_ProvideStationInventoryInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideStationInventoryInformation  implements java.io.Serializable {
    private java.lang.String MSG_StationInventoryRequest;

    public OP_ProvideStationInventoryInformation() {
    }

    public OP_ProvideStationInventoryInformation(
           java.lang.String MSG_StationInventoryRequest) {
           this.MSG_StationInventoryRequest = MSG_StationInventoryRequest;
    }


    /**
     * Gets the MSG_StationInventoryRequest value for this OP_ProvideStationInventoryInformation.
     * 
     * @return MSG_StationInventoryRequest
     */
    public java.lang.String getMSG_StationInventoryRequest() {
        return MSG_StationInventoryRequest;
    }


    /**
     * Sets the MSG_StationInventoryRequest value for this OP_ProvideStationInventoryInformation.
     * 
     * @param MSG_StationInventoryRequest
     */
    public void setMSG_StationInventoryRequest(java.lang.String MSG_StationInventoryRequest) {
        this.MSG_StationInventoryRequest = MSG_StationInventoryRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideStationInventoryInformation)) return false;
        OP_ProvideStationInventoryInformation other = (OP_ProvideStationInventoryInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_StationInventoryRequest==null && other.getMSG_StationInventoryRequest()==null) || 
             (this.MSG_StationInventoryRequest!=null &&
              this.MSG_StationInventoryRequest.equals(other.getMSG_StationInventoryRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_StationInventoryRequest() != null) {
            _hashCode += getMSG_StationInventoryRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideStationInventoryInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideStationInventoryInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_StationInventoryRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_StationInventoryRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
