/**
 * OP_ShareUdotVehicleClassificationDataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareUdotVehicleClassificationDataResponse  implements java.io.Serializable {
    private java.lang.String OP_ShareUdotVehicleClassificationDataResult;

    public OP_ShareUdotVehicleClassificationDataResponse() {
    }

    public OP_ShareUdotVehicleClassificationDataResponse(
           java.lang.String OP_ShareUdotVehicleClassificationDataResult) {
           this.OP_ShareUdotVehicleClassificationDataResult = OP_ShareUdotVehicleClassificationDataResult;
    }


    /**
     * Gets the OP_ShareUdotVehicleClassificationDataResult value for this OP_ShareUdotVehicleClassificationDataResponse.
     * 
     * @return OP_ShareUdotVehicleClassificationDataResult
     */
    public java.lang.String getOP_ShareUdotVehicleClassificationDataResult() {
        return OP_ShareUdotVehicleClassificationDataResult;
    }


    /**
     * Sets the OP_ShareUdotVehicleClassificationDataResult value for this OP_ShareUdotVehicleClassificationDataResponse.
     * 
     * @param OP_ShareUdotVehicleClassificationDataResult
     */
    public void setOP_ShareUdotVehicleClassificationDataResult(java.lang.String OP_ShareUdotVehicleClassificationDataResult) {
        this.OP_ShareUdotVehicleClassificationDataResult = OP_ShareUdotVehicleClassificationDataResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareUdotVehicleClassificationDataResponse)) return false;
        OP_ShareUdotVehicleClassificationDataResponse other = (OP_ShareUdotVehicleClassificationDataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ShareUdotVehicleClassificationDataResult==null && other.getOP_ShareUdotVehicleClassificationDataResult()==null) || 
             (this.OP_ShareUdotVehicleClassificationDataResult!=null &&
              this.OP_ShareUdotVehicleClassificationDataResult.equals(other.getOP_ShareUdotVehicleClassificationDataResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ShareUdotVehicleClassificationDataResult() != null) {
            _hashCode += getOP_ShareUdotVehicleClassificationDataResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareUdotVehicleClassificationDataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareUdotVehicleClassificationDataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ShareUdotVehicleClassificationDataResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ShareUdotVehicleClassificationDataResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
