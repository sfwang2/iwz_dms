/**
 * OP_ProvideLinkStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideLinkStatus  implements java.io.Serializable {
    private java.lang.String MSG_LinkStatusRequest;

    public OP_ProvideLinkStatus() {
    }

    public OP_ProvideLinkStatus(
           java.lang.String MSG_LinkStatusRequest) {
           this.MSG_LinkStatusRequest = MSG_LinkStatusRequest;
    }


    /**
     * Gets the MSG_LinkStatusRequest value for this OP_ProvideLinkStatus.
     * 
     * @return MSG_LinkStatusRequest
     */
    public java.lang.String getMSG_LinkStatusRequest() {
        return MSG_LinkStatusRequest;
    }


    /**
     * Sets the MSG_LinkStatusRequest value for this OP_ProvideLinkStatus.
     * 
     * @param MSG_LinkStatusRequest
     */
    public void setMSG_LinkStatusRequest(java.lang.String MSG_LinkStatusRequest) {
        this.MSG_LinkStatusRequest = MSG_LinkStatusRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideLinkStatus)) return false;
        OP_ProvideLinkStatus other = (OP_ProvideLinkStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_LinkStatusRequest==null && other.getMSG_LinkStatusRequest()==null) || 
             (this.MSG_LinkStatusRequest!=null &&
              this.MSG_LinkStatusRequest.equals(other.getMSG_LinkStatusRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_LinkStatusRequest() != null) {
            _hashCode += getMSG_LinkStatusRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideLinkStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideLinkStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_LinkStatusRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_LinkStatusRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
