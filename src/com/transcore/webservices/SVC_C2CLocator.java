/**
 * SVC_C2CLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class SVC_C2CLocator extends org.apache.axis.client.Service implements com.transcore.webservices.SVC_C2C {

    public SVC_C2CLocator() {
    }


    public SVC_C2CLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SVC_C2CLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SVC_C2CSoap
    private java.lang.String SVC_C2CSoap_address = "http://172.16.16.11/TransSuite.C2C/SVC_C2C.asmx";

    public java.lang.String getSVC_C2CSoapAddress() {
        return SVC_C2CSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SVC_C2CSoapWSDDServiceName = "SVC_C2CSoap";

    public java.lang.String getSVC_C2CSoapWSDDServiceName() {
        return SVC_C2CSoapWSDDServiceName;
    }

    public void setSVC_C2CSoapWSDDServiceName(java.lang.String name) {
        SVC_C2CSoapWSDDServiceName = name;
    }

    public com.transcore.webservices.SVC_C2CSoap getSVC_C2CSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SVC_C2CSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSVC_C2CSoap(endpoint);
    }

    public com.transcore.webservices.SVC_C2CSoap getSVC_C2CSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.transcore.webservices.SVC_C2CSoapStub _stub = new com.transcore.webservices.SVC_C2CSoapStub(portAddress, this);
            _stub.setPortName(getSVC_C2CSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSVC_C2CSoapEndpointAddress(java.lang.String address) {
        SVC_C2CSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.transcore.webservices.SVC_C2CSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.transcore.webservices.SVC_C2CSoapStub _stub = new com.transcore.webservices.SVC_C2CSoapStub(new java.net.URL(SVC_C2CSoap_address), this);
                _stub.setPortName(getSVC_C2CSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SVC_C2CSoap".equals(inputPortName)) {
            return getSVC_C2CSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://transcore.com/webservices/", "SVC_C2C");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://transcore.com/webservices/", "SVC_C2CSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SVC_C2CSoap".equals(portName)) {
            setSVC_C2CSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
