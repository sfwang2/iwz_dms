/**
 * OP_ProvideRouteDataInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideRouteDataInformation  implements java.io.Serializable {
    private java.lang.String MSG_RouteDataRequest;

    public OP_ProvideRouteDataInformation() {
    }

    public OP_ProvideRouteDataInformation(
           java.lang.String MSG_RouteDataRequest) {
           this.MSG_RouteDataRequest = MSG_RouteDataRequest;
    }


    /**
     * Gets the MSG_RouteDataRequest value for this OP_ProvideRouteDataInformation.
     * 
     * @return MSG_RouteDataRequest
     */
    public java.lang.String getMSG_RouteDataRequest() {
        return MSG_RouteDataRequest;
    }


    /**
     * Sets the MSG_RouteDataRequest value for this OP_ProvideRouteDataInformation.
     * 
     * @param MSG_RouteDataRequest
     */
    public void setMSG_RouteDataRequest(java.lang.String MSG_RouteDataRequest) {
        this.MSG_RouteDataRequest = MSG_RouteDataRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideRouteDataInformation)) return false;
        OP_ProvideRouteDataInformation other = (OP_ProvideRouteDataInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_RouteDataRequest==null && other.getMSG_RouteDataRequest()==null) || 
             (this.MSG_RouteDataRequest!=null &&
              this.MSG_RouteDataRequest.equals(other.getMSG_RouteDataRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_RouteDataRequest() != null) {
            _hashCode += getMSG_RouteDataRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideRouteDataInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideRouteDataInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_RouteDataRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_RouteDataRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
