/**
 * OP_ShareCCTVInventoryInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareCCTVInventoryInformationResponse  implements java.io.Serializable {
    private java.lang.String OP_ShareCCTVInventoryInformationResult;

    public OP_ShareCCTVInventoryInformationResponse() {
    }

    public OP_ShareCCTVInventoryInformationResponse(
           java.lang.String OP_ShareCCTVInventoryInformationResult) {
           this.OP_ShareCCTVInventoryInformationResult = OP_ShareCCTVInventoryInformationResult;
    }


    /**
     * Gets the OP_ShareCCTVInventoryInformationResult value for this OP_ShareCCTVInventoryInformationResponse.
     * 
     * @return OP_ShareCCTVInventoryInformationResult
     */
    public java.lang.String getOP_ShareCCTVInventoryInformationResult() {
        return OP_ShareCCTVInventoryInformationResult;
    }


    /**
     * Sets the OP_ShareCCTVInventoryInformationResult value for this OP_ShareCCTVInventoryInformationResponse.
     * 
     * @param OP_ShareCCTVInventoryInformationResult
     */
    public void setOP_ShareCCTVInventoryInformationResult(java.lang.String OP_ShareCCTVInventoryInformationResult) {
        this.OP_ShareCCTVInventoryInformationResult = OP_ShareCCTVInventoryInformationResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareCCTVInventoryInformationResponse)) return false;
        OP_ShareCCTVInventoryInformationResponse other = (OP_ShareCCTVInventoryInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ShareCCTVInventoryInformationResult==null && other.getOP_ShareCCTVInventoryInformationResult()==null) || 
             (this.OP_ShareCCTVInventoryInformationResult!=null &&
              this.OP_ShareCCTVInventoryInformationResult.equals(other.getOP_ShareCCTVInventoryInformationResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ShareCCTVInventoryInformationResult() != null) {
            _hashCode += getOP_ShareCCTVInventoryInformationResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareCCTVInventoryInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareCCTVInventoryInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ShareCCTVInventoryInformationResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ShareCCTVInventoryInformationResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
