/**
 * OP_ShareTrafficDetectorDataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareTrafficDetectorDataResponse  implements java.io.Serializable {
    private java.lang.String OP_ShareTrafficDetectorDataResult;

    public OP_ShareTrafficDetectorDataResponse() {
    }

    public OP_ShareTrafficDetectorDataResponse(
           java.lang.String OP_ShareTrafficDetectorDataResult) {
           this.OP_ShareTrafficDetectorDataResult = OP_ShareTrafficDetectorDataResult;
    }


    /**
     * Gets the OP_ShareTrafficDetectorDataResult value for this OP_ShareTrafficDetectorDataResponse.
     * 
     * @return OP_ShareTrafficDetectorDataResult
     */
    public java.lang.String getOP_ShareTrafficDetectorDataResult() {
        return OP_ShareTrafficDetectorDataResult;
    }


    /**
     * Sets the OP_ShareTrafficDetectorDataResult value for this OP_ShareTrafficDetectorDataResponse.
     * 
     * @param OP_ShareTrafficDetectorDataResult
     */
    public void setOP_ShareTrafficDetectorDataResult(java.lang.String OP_ShareTrafficDetectorDataResult) {
        this.OP_ShareTrafficDetectorDataResult = OP_ShareTrafficDetectorDataResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareTrafficDetectorDataResponse)) return false;
        OP_ShareTrafficDetectorDataResponse other = (OP_ShareTrafficDetectorDataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ShareTrafficDetectorDataResult==null && other.getOP_ShareTrafficDetectorDataResult()==null) || 
             (this.OP_ShareTrafficDetectorDataResult!=null &&
              this.OP_ShareTrafficDetectorDataResult.equals(other.getOP_ShareTrafficDetectorDataResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ShareTrafficDetectorDataResult() != null) {
            _hashCode += getOP_ShareTrafficDetectorDataResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareTrafficDetectorDataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareTrafficDetectorDataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ShareTrafficDetectorDataResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ShareTrafficDetectorDataResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
