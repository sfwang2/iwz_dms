/**
 * OP_ShareUdotVehicleClassificationData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareUdotVehicleClassificationData  implements java.io.Serializable {
    private java.lang.String MSG_UdotVehicleClassificationDataRequest;

    public OP_ShareUdotVehicleClassificationData() {
    }

    public OP_ShareUdotVehicleClassificationData(
           java.lang.String MSG_UdotVehicleClassificationDataRequest) {
           this.MSG_UdotVehicleClassificationDataRequest = MSG_UdotVehicleClassificationDataRequest;
    }


    /**
     * Gets the MSG_UdotVehicleClassificationDataRequest value for this OP_ShareUdotVehicleClassificationData.
     * 
     * @return MSG_UdotVehicleClassificationDataRequest
     */
    public java.lang.String getMSG_UdotVehicleClassificationDataRequest() {
        return MSG_UdotVehicleClassificationDataRequest;
    }


    /**
     * Sets the MSG_UdotVehicleClassificationDataRequest value for this OP_ShareUdotVehicleClassificationData.
     * 
     * @param MSG_UdotVehicleClassificationDataRequest
     */
    public void setMSG_UdotVehicleClassificationDataRequest(java.lang.String MSG_UdotVehicleClassificationDataRequest) {
        this.MSG_UdotVehicleClassificationDataRequest = MSG_UdotVehicleClassificationDataRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareUdotVehicleClassificationData)) return false;
        OP_ShareUdotVehicleClassificationData other = (OP_ShareUdotVehicleClassificationData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_UdotVehicleClassificationDataRequest==null && other.getMSG_UdotVehicleClassificationDataRequest()==null) || 
             (this.MSG_UdotVehicleClassificationDataRequest!=null &&
              this.MSG_UdotVehicleClassificationDataRequest.equals(other.getMSG_UdotVehicleClassificationDataRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_UdotVehicleClassificationDataRequest() != null) {
            _hashCode += getMSG_UdotVehicleClassificationDataRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareUdotVehicleClassificationData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareUdotVehicleClassificationData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_UdotVehicleClassificationDataRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_UdotVehicleClassificationDataRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
