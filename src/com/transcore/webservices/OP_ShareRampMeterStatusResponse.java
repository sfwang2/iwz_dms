/**
 * OP_ShareRampMeterStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareRampMeterStatusResponse  implements java.io.Serializable {
    private java.lang.String OP_ShareRampMeterStatusResult;

    public OP_ShareRampMeterStatusResponse() {
    }

    public OP_ShareRampMeterStatusResponse(
           java.lang.String OP_ShareRampMeterStatusResult) {
           this.OP_ShareRampMeterStatusResult = OP_ShareRampMeterStatusResult;
    }


    /**
     * Gets the OP_ShareRampMeterStatusResult value for this OP_ShareRampMeterStatusResponse.
     * 
     * @return OP_ShareRampMeterStatusResult
     */
    public java.lang.String getOP_ShareRampMeterStatusResult() {
        return OP_ShareRampMeterStatusResult;
    }


    /**
     * Sets the OP_ShareRampMeterStatusResult value for this OP_ShareRampMeterStatusResponse.
     * 
     * @param OP_ShareRampMeterStatusResult
     */
    public void setOP_ShareRampMeterStatusResult(java.lang.String OP_ShareRampMeterStatusResult) {
        this.OP_ShareRampMeterStatusResult = OP_ShareRampMeterStatusResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareRampMeterStatusResponse)) return false;
        OP_ShareRampMeterStatusResponse other = (OP_ShareRampMeterStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ShareRampMeterStatusResult==null && other.getOP_ShareRampMeterStatusResult()==null) || 
             (this.OP_ShareRampMeterStatusResult!=null &&
              this.OP_ShareRampMeterStatusResult.equals(other.getOP_ShareRampMeterStatusResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ShareRampMeterStatusResult() != null) {
            _hashCode += getOP_ShareRampMeterStatusResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareRampMeterStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareRampMeterStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ShareRampMeterStatusResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ShareRampMeterStatusResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
