/**
 * OP_ProvideEventIndexMessageResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideEventIndexMessageResponse  implements java.io.Serializable {
    private com.transcore.webservices.OP_ProvideEventIndexMessageResponseOP_ProvideEventIndexMessageResult OP_ProvideEventIndexMessageResult;

    public OP_ProvideEventIndexMessageResponse() {
    }

    public OP_ProvideEventIndexMessageResponse(
           com.transcore.webservices.OP_ProvideEventIndexMessageResponseOP_ProvideEventIndexMessageResult OP_ProvideEventIndexMessageResult) {
           this.OP_ProvideEventIndexMessageResult = OP_ProvideEventIndexMessageResult;
    }


    /**
     * Gets the OP_ProvideEventIndexMessageResult value for this OP_ProvideEventIndexMessageResponse.
     * 
     * @return OP_ProvideEventIndexMessageResult
     */
    public com.transcore.webservices.OP_ProvideEventIndexMessageResponseOP_ProvideEventIndexMessageResult getOP_ProvideEventIndexMessageResult() {
        return OP_ProvideEventIndexMessageResult;
    }


    /**
     * Sets the OP_ProvideEventIndexMessageResult value for this OP_ProvideEventIndexMessageResponse.
     * 
     * @param OP_ProvideEventIndexMessageResult
     */
    public void setOP_ProvideEventIndexMessageResult(com.transcore.webservices.OP_ProvideEventIndexMessageResponseOP_ProvideEventIndexMessageResult OP_ProvideEventIndexMessageResult) {
        this.OP_ProvideEventIndexMessageResult = OP_ProvideEventIndexMessageResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideEventIndexMessageResponse)) return false;
        OP_ProvideEventIndexMessageResponse other = (OP_ProvideEventIndexMessageResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideEventIndexMessageResult==null && other.getOP_ProvideEventIndexMessageResult()==null) || 
             (this.OP_ProvideEventIndexMessageResult!=null &&
              this.OP_ProvideEventIndexMessageResult.equals(other.getOP_ProvideEventIndexMessageResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideEventIndexMessageResult() != null) {
            _hashCode += getOP_ProvideEventIndexMessageResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideEventIndexMessageResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideEventIndexMessageResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideEventIndexMessageResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideEventIndexMessageResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">>OP_ProvideEventIndexMessageResponse>OP_ProvideEventIndexMessageResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
