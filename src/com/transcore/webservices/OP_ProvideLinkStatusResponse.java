/**
 * OP_ProvideLinkStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideLinkStatusResponse  implements java.io.Serializable {
    private java.lang.String OP_ProvideLinkStatusResult;

    public OP_ProvideLinkStatusResponse() {
    }

    public OP_ProvideLinkStatusResponse(
           java.lang.String OP_ProvideLinkStatusResult) {
           this.OP_ProvideLinkStatusResult = OP_ProvideLinkStatusResult;
    }


    /**
     * Gets the OP_ProvideLinkStatusResult value for this OP_ProvideLinkStatusResponse.
     * 
     * @return OP_ProvideLinkStatusResult
     */
    public java.lang.String getOP_ProvideLinkStatusResult() {
        return OP_ProvideLinkStatusResult;
    }


    /**
     * Sets the OP_ProvideLinkStatusResult value for this OP_ProvideLinkStatusResponse.
     * 
     * @param OP_ProvideLinkStatusResult
     */
    public void setOP_ProvideLinkStatusResult(java.lang.String OP_ProvideLinkStatusResult) {
        this.OP_ProvideLinkStatusResult = OP_ProvideLinkStatusResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideLinkStatusResponse)) return false;
        OP_ProvideLinkStatusResponse other = (OP_ProvideLinkStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideLinkStatusResult==null && other.getOP_ProvideLinkStatusResult()==null) || 
             (this.OP_ProvideLinkStatusResult!=null &&
              this.OP_ProvideLinkStatusResult.equals(other.getOP_ProvideLinkStatusResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideLinkStatusResult() != null) {
            _hashCode += getOP_ProvideLinkStatusResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideLinkStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideLinkStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideLinkStatusResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideLinkStatusResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
