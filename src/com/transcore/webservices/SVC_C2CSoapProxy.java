package com.transcore.webservices;

public class SVC_C2CSoapProxy implements com.transcore.webservices.SVC_C2CSoap {
  private String _endpoint = null;
  private com.transcore.webservices.SVC_C2CSoap sVC_C2CSoap = null;
  
  public SVC_C2CSoapProxy() {
    _initSVC_C2CSoapProxy();
  }
  
  public SVC_C2CSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSVC_C2CSoapProxy();
  }
  
  private void _initSVC_C2CSoapProxy() {
    try {
      sVC_C2CSoap = (new com.transcore.webservices.SVC_C2CLocator()).getSVC_C2CSoap();
      if (sVC_C2CSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sVC_C2CSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sVC_C2CSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sVC_C2CSoap != null)
      ((javax.xml.rpc.Stub)sVC_C2CSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.transcore.webservices.SVC_C2CSoap getSVC_C2CSoap() {
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap;
  }
  
  public java.lang.String OP_ShareDMSInventoryInformation(java.lang.String MSG_DMSInventoryRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareDMSInventoryInformation(MSG_DMSInventoryRequest);
  }
  
  public java.lang.String OP_ShareDMSStatusInformation(java.lang.String MSG_DMSStatusRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareDMSStatusInformation(MSG_DMSStatusRequest);
  }
  
  public java.lang.String OP_ShareDMSControl(java.lang.String MSG_DMSControlRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareDMSControl(MSG_DMSControlRequest);
  }
  
  public java.lang.String OP_ShareESSInventoryInformation(java.lang.String MSG_ESSInventoryRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareESSInventoryInformation(MSG_ESSInventoryRequest);
  }
  
  public java.lang.String OP_ShareESSStatusInformation(java.lang.String MSG_ESSStatusRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareESSStatusInformation(MSG_ESSStatusRequest);
  }
  
  public com.transcore.webservices.OP_ShareTATSStatusInformationResponseOP_ShareTATSStatusInformationResult OP_ShareTATSStatusInformation(java.lang.String MSG_TATSStatusRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareTATSStatusInformation(MSG_TATSStatusRequest);
  }
  
  public java.lang.String OP_ShareCCTVInventoryInformation(java.lang.String MSG_CCTVInventoryRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareCCTVInventoryInformation(MSG_CCTVInventoryRequest);
  }
  
  public java.lang.String OP_ShareTrafficDetectorInventoryInformation(java.lang.String MSG_TrafficDetectorInventoryRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareTrafficDetectorInventoryInformation(MSG_TrafficDetectorInventoryRequest);
  }
  
  public java.lang.String OP_ShareTrafficDetectorData(java.lang.String MSG_TrafficDetectorDataRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareTrafficDetectorData(MSG_TrafficDetectorDataRequest);
  }
  
  public java.lang.String OP_ShareUdotVehicleClassificationData(java.lang.String MSG_UdotVehicleClassificationDataRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareUdotVehicleClassificationData(MSG_UdotVehicleClassificationDataRequest);
  }
  
  public java.lang.String OP_ShareRampMeterInventoryInformation(java.lang.String MSG_RampMeterInventoryRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareRampMeterInventoryInformation(MSG_RampMeterInventoryRequest);
  }
  
  public java.lang.String OP_ShareRampMeterStatus(java.lang.String MSG_RampMeterStatusRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareRampMeterStatus(MSG_RampMeterStatusRequest);
  }
  
  public java.lang.String OP_ProvideTrafficNetworkInventoryInformation(java.lang.String MSG_TrafficNetworkRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideTrafficNetworkInventoryInformation(MSG_TrafficNetworkRequest);
  }
  
  public java.lang.String OP_ProvideNodeStatus(java.lang.String MSG_NodeStatusRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideNodeStatus(MSG_NodeStatusRequest);
  }
  
  public java.lang.String OP_ProvideLinkStatus(java.lang.String MSG_LinkStatusRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideLinkStatus(MSG_LinkStatusRequest);
  }
  
  public java.lang.String OP_ProvideLinkData(java.lang.String MSG_LinkDataRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideLinkData(MSG_LinkDataRequest);
  }
  
  public java.lang.String OP_ProvideRouteInventoryInformation(java.lang.String MSG_RouteInventoryRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideRouteInventoryInformation(MSG_RouteInventoryRequest);
  }
  
  public java.lang.String OP_ProvideRouteDataInformation(java.lang.String MSG_RouteDataRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideRouteDataInformation(MSG_RouteDataRequest);
  }
  
  public java.lang.String OP_ProvideStationInventoryInformation(java.lang.String MSG_StationInventoryRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideStationInventoryInformation(MSG_StationInventoryRequest);
  }
  
  public java.lang.String OP_ProvideBasicEventUpdate(java.lang.String MSG_BasicEventUpdateRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideBasicEventUpdate(MSG_BasicEventUpdateRequest);
  }
  
  public java.lang.String OP_ProvideFullEventUpdate(java.lang.String MSG_FullEventUpdateRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideFullEventUpdate(MSG_FullEventUpdateRequest);
  }
  
  public com.transcore.webservices.TmddV3ResponseTmddV3Result OP_ShareFullEventUpdate(java.lang.String MSG_FullEventUpdateRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ShareFullEventUpdate(MSG_FullEventUpdateRequest);
  }
  
  public com.transcore.webservices.OP_ProvideEventFullEventUpdateResponseOP_ProvideEventFullEventUpdateResult OP_ProvideEventFullEventUpdate(java.lang.String MSG_FullEventUpdateRequest, java.lang.String MSG_EventID, int MSG_UpdateNumber) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideEventFullEventUpdate(MSG_FullEventUpdateRequest, MSG_EventID, MSG_UpdateNumber);
  }
  
  public com.transcore.webservices.OP_ProvideEventIndexMessageResponseOP_ProvideEventIndexMessageResult OP_ProvideEventIndexMessage(java.lang.String MSG_EventIndexMessageRequest) throws java.rmi.RemoteException{
    if (sVC_C2CSoap == null)
      _initSVC_C2CSoapProxy();
    return sVC_C2CSoap.OP_ProvideEventIndexMessage(MSG_EventIndexMessageRequest);
  }
  
  
}