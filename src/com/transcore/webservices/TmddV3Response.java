/**
 * TmddV3Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class TmddV3Response  implements java.io.Serializable {
    private com.transcore.webservices.TmddV3ResponseTmddV3Result tmddV3Result;

    public TmddV3Response() {
    }

    public TmddV3Response(
           com.transcore.webservices.TmddV3ResponseTmddV3Result tmddV3Result) {
           this.tmddV3Result = tmddV3Result;
    }


    /**
     * Gets the tmddV3Result value for this TmddV3Response.
     * 
     * @return tmddV3Result
     */
    public com.transcore.webservices.TmddV3ResponseTmddV3Result getTmddV3Result() {
        return tmddV3Result;
    }


    /**
     * Sets the tmddV3Result value for this TmddV3Response.
     * 
     * @param tmddV3Result
     */
    public void setTmddV3Result(com.transcore.webservices.TmddV3ResponseTmddV3Result tmddV3Result) {
        this.tmddV3Result = tmddV3Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TmddV3Response)) return false;
        TmddV3Response other = (TmddV3Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tmddV3Result==null && other.getTmddV3Result()==null) || 
             (this.tmddV3Result!=null &&
              this.tmddV3Result.equals(other.getTmddV3Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTmddV3Result() != null) {
            _hashCode += getTmddV3Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TmddV3Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">TmddV3Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tmddV3Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "TmddV3Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">>TmddV3Response>TmddV3Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
