/**
 * OP_ShareTrafficDetectorData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ShareTrafficDetectorData  implements java.io.Serializable {
    private java.lang.String MSG_TrafficDetectorDataRequest;

    public OP_ShareTrafficDetectorData() {
    }

    public OP_ShareTrafficDetectorData(
           java.lang.String MSG_TrafficDetectorDataRequest) {
           this.MSG_TrafficDetectorDataRequest = MSG_TrafficDetectorDataRequest;
    }


    /**
     * Gets the MSG_TrafficDetectorDataRequest value for this OP_ShareTrafficDetectorData.
     * 
     * @return MSG_TrafficDetectorDataRequest
     */
    public java.lang.String getMSG_TrafficDetectorDataRequest() {
        return MSG_TrafficDetectorDataRequest;
    }


    /**
     * Sets the MSG_TrafficDetectorDataRequest value for this OP_ShareTrafficDetectorData.
     * 
     * @param MSG_TrafficDetectorDataRequest
     */
    public void setMSG_TrafficDetectorDataRequest(java.lang.String MSG_TrafficDetectorDataRequest) {
        this.MSG_TrafficDetectorDataRequest = MSG_TrafficDetectorDataRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ShareTrafficDetectorData)) return false;
        OP_ShareTrafficDetectorData other = (OP_ShareTrafficDetectorData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MSG_TrafficDetectorDataRequest==null && other.getMSG_TrafficDetectorDataRequest()==null) || 
             (this.MSG_TrafficDetectorDataRequest!=null &&
              this.MSG_TrafficDetectorDataRequest.equals(other.getMSG_TrafficDetectorDataRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMSG_TrafficDetectorDataRequest() != null) {
            _hashCode += getMSG_TrafficDetectorDataRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ShareTrafficDetectorData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ShareTrafficDetectorData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG_TrafficDetectorDataRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "MSG_TrafficDetectorDataRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
