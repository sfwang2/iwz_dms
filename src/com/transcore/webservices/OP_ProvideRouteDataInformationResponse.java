/**
 * OP_ProvideRouteDataInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideRouteDataInformationResponse  implements java.io.Serializable {
    private java.lang.String OP_ProvideRouteDataInformationResult;

    public OP_ProvideRouteDataInformationResponse() {
    }

    public OP_ProvideRouteDataInformationResponse(
           java.lang.String OP_ProvideRouteDataInformationResult) {
           this.OP_ProvideRouteDataInformationResult = OP_ProvideRouteDataInformationResult;
    }


    /**
     * Gets the OP_ProvideRouteDataInformationResult value for this OP_ProvideRouteDataInformationResponse.
     * 
     * @return OP_ProvideRouteDataInformationResult
     */
    public java.lang.String getOP_ProvideRouteDataInformationResult() {
        return OP_ProvideRouteDataInformationResult;
    }


    /**
     * Sets the OP_ProvideRouteDataInformationResult value for this OP_ProvideRouteDataInformationResponse.
     * 
     * @param OP_ProvideRouteDataInformationResult
     */
    public void setOP_ProvideRouteDataInformationResult(java.lang.String OP_ProvideRouteDataInformationResult) {
        this.OP_ProvideRouteDataInformationResult = OP_ProvideRouteDataInformationResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideRouteDataInformationResponse)) return false;
        OP_ProvideRouteDataInformationResponse other = (OP_ProvideRouteDataInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideRouteDataInformationResult==null && other.getOP_ProvideRouteDataInformationResult()==null) || 
             (this.OP_ProvideRouteDataInformationResult!=null &&
              this.OP_ProvideRouteDataInformationResult.equals(other.getOP_ProvideRouteDataInformationResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideRouteDataInformationResult() != null) {
            _hashCode += getOP_ProvideRouteDataInformationResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideRouteDataInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideRouteDataInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideRouteDataInformationResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideRouteDataInformationResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
