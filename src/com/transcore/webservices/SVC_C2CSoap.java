/**
 * SVC_C2CSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public interface SVC_C2CSoap extends java.rmi.Remote {

    /**
     * Returns MSG_DMSInventoryList as XML string
     */
    public java.lang.String OP_ShareDMSInventoryInformation(java.lang.String MSG_DMSInventoryRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_DMSStatusList as XML string
     */
    public java.lang.String OP_ShareDMSStatusInformation(java.lang.String MSG_DMSStatusRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_DMSControlResponse as XML string
     */
    public java.lang.String OP_ShareDMSControl(java.lang.String MSG_DMSControlRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_ESSInventoryList as XML string
     */
    public java.lang.String OP_ShareESSInventoryInformation(java.lang.String MSG_ESSInventoryRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_ESSStatusList as XML string
     */
    public java.lang.String OP_ShareESSStatusInformation(java.lang.String MSG_ESSStatusRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_TATSStatusList as XML document
     */
    public com.transcore.webservices.OP_ShareTATSStatusInformationResponseOP_ShareTATSStatusInformationResult OP_ShareTATSStatusInformation(java.lang.String MSG_TATSStatusRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_CCTVInventoryList as XML string
     */
    public java.lang.String OP_ShareCCTVInventoryInformation(java.lang.String MSG_CCTVInventoryRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_DetectorInventoryList as XML string
     */
    public java.lang.String OP_ShareTrafficDetectorInventoryInformation(java.lang.String MSG_TrafficDetectorInventoryRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_DetectorDataList as XML string
     */
    public java.lang.String OP_ShareTrafficDetectorData(java.lang.String MSG_TrafficDetectorDataRequest) throws java.rmi.RemoteException;

    /**
     * Returns 15 minute vehicle classification data as XML string
     */
    public java.lang.String OP_ShareUdotVehicleClassificationData(java.lang.String MSG_UdotVehicleClassificationDataRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_RampMeterInventoryList as XML string
     */
    public java.lang.String OP_ShareRampMeterInventoryInformation(java.lang.String MSG_RampMeterInventoryRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_RampMeterStatusList as XML string
     */
    public java.lang.String OP_ShareRampMeterStatus(java.lang.String MSG_RampMeterStatusRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_TrafficNetworkInventoryResponseList as XML string
     */
    public java.lang.String OP_ProvideTrafficNetworkInventoryInformation(java.lang.String MSG_TrafficNetworkRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_NodeStatusList as XML string
     */
    public java.lang.String OP_ProvideNodeStatus(java.lang.String MSG_NodeStatusRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_LinkStatusList as XML string
     */
    public java.lang.String OP_ProvideLinkStatus(java.lang.String MSG_LinkStatusRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_LinkDataList as XML string
     */
    public java.lang.String OP_ProvideLinkData(java.lang.String MSG_LinkDataRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_RouteInventoryList as XML string
     */
    public java.lang.String OP_ProvideRouteInventoryInformation(java.lang.String MSG_RouteInventoryRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_RouteDataList as XML string
     */
    public java.lang.String OP_ProvideRouteDataInformation(java.lang.String MSG_RouteDataRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_StationInventoryList as XML string
     */
    public java.lang.String OP_ProvideStationInventoryInformation(java.lang.String MSG_StationInventoryRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_TranscoreBasicEventUpdateList as XML string
     */
    public java.lang.String OP_ProvideBasicEventUpdate(java.lang.String MSG_BasicEventUpdateRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_TranscoreFullEventUpdateList as XML string
     */
    public java.lang.String OP_ProvideFullEventUpdate(java.lang.String MSG_FullEventUpdateRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_FullEventUpdate Tmdd V3 as XmlDocument
     */
    public com.transcore.webservices.TmddV3ResponseTmddV3Result OP_ShareFullEventUpdate(java.lang.String MSG_FullEventUpdateRequest) throws java.rmi.RemoteException;

    /**
     * Returns MSG_TranscoreEventFullEventUpdateList as XML Document
     */
    public com.transcore.webservices.OP_ProvideEventFullEventUpdateResponseOP_ProvideEventFullEventUpdateResult OP_ProvideEventFullEventUpdate(java.lang.String MSG_FullEventUpdateRequest, java.lang.String MSG_EventID, int MSG_UpdateNumber) throws java.rmi.RemoteException;

    /**
     * Returns MSG_TranscoreEventIndexMessageList as XML Document
     */
    public com.transcore.webservices.OP_ProvideEventIndexMessageResponseOP_ProvideEventIndexMessageResult OP_ProvideEventIndexMessage(java.lang.String MSG_EventIndexMessageRequest) throws java.rmi.RemoteException;
}
