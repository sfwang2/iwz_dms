/**
 * OP_ProvideStationInventoryInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.transcore.webservices;

public class OP_ProvideStationInventoryInformationResponse  implements java.io.Serializable {
    private java.lang.String OP_ProvideStationInventoryInformationResult;

    public OP_ProvideStationInventoryInformationResponse() {
    }

    public OP_ProvideStationInventoryInformationResponse(
           java.lang.String OP_ProvideStationInventoryInformationResult) {
           this.OP_ProvideStationInventoryInformationResult = OP_ProvideStationInventoryInformationResult;
    }


    /**
     * Gets the OP_ProvideStationInventoryInformationResult value for this OP_ProvideStationInventoryInformationResponse.
     * 
     * @return OP_ProvideStationInventoryInformationResult
     */
    public java.lang.String getOP_ProvideStationInventoryInformationResult() {
        return OP_ProvideStationInventoryInformationResult;
    }


    /**
     * Sets the OP_ProvideStationInventoryInformationResult value for this OP_ProvideStationInventoryInformationResponse.
     * 
     * @param OP_ProvideStationInventoryInformationResult
     */
    public void setOP_ProvideStationInventoryInformationResult(java.lang.String OP_ProvideStationInventoryInformationResult) {
        this.OP_ProvideStationInventoryInformationResult = OP_ProvideStationInventoryInformationResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OP_ProvideStationInventoryInformationResponse)) return false;
        OP_ProvideStationInventoryInformationResponse other = (OP_ProvideStationInventoryInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OP_ProvideStationInventoryInformationResult==null && other.getOP_ProvideStationInventoryInformationResult()==null) || 
             (this.OP_ProvideStationInventoryInformationResult!=null &&
              this.OP_ProvideStationInventoryInformationResult.equals(other.getOP_ProvideStationInventoryInformationResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOP_ProvideStationInventoryInformationResult() != null) {
            _hashCode += getOP_ProvideStationInventoryInformationResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OP_ProvideStationInventoryInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://transcore.com/webservices/", ">OP_ProvideStationInventoryInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OP_ProvideStationInventoryInformationResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://transcore.com/webservices/", "OP_ProvideStationInventoryInformationResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
