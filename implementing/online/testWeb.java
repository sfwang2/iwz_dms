package online;

import java.io.File;
import java.text.SimpleDateFormat;

import com.transcore.webservices.SVC_C2CSoapProxy;

import utilMethods.UtilityMethods;
import utilMethods.VSLControlClass;

public class testWeb {

	private final static String DETECTOR_ID = "I-35 SB @ NE 158th Ave"; 
	private final static String DETECTOR_ID2 = "I-35 SB @ MP 94"; 
	private final static String DETECTOR_ID3 = "I-35 SB @ NE 126th Ave"; 
	private final static String DETECTOR_ID4 = "I-35 NB at 1st AVE ANKENY-SB"; 
	
	
	private final static String INTERNAL_ID = "310";
	public static void main(String[] args) throws Exception {
		SVC_C2CSoapProxy proxy = new SVC_C2CSoapProxy();

//		String msg1 = "<dMSControlRequest><organization-information><organization-id>35006001</organization-id><organization-name>Ames</organization-name><center-id>1</center-id></organization-information><device-id>309</device-id><request-id>Request0001</request-id><operator-id>intrans</operator-id><user-id>IOWASIMS\\exttis\\USER</user-id><password>IOWAt1s</password><dms-beacon-control>0</dms-beacon-control>";
		String msg1 = "<dMSControlRequest><organization-information><organization-id>35006001</organization-id><organization-name>Ames</organization-name><center-id>1</center-id></organization-information><device-id>309</device-id><request-id>intrans</request-id><operator-id>intrans</operator-id><user-id>IOWASIMS\\exttis\\USER</user-id><password>IOWAt1s</password><dms-beacon-control>0</dms-beacon-control>";

		String msg2 = "<dms-message>50-mph</dms-message>";
		String msg3 = "<command-request-priority>1</command-request-priority><request-date-time><local-date>20161014</local-date><local-time>142000</local-time><utc-offset>-0500</utc-offset></request-date-time><command-end-time>144000</command-end-time></dMSControlRequest>";

		String today = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());	
		String InternalID = "310";
		String Msg = "55 mph";
		String startTime = "141000";
		String endTime = "145000";
		String MSG_DMSControlRequest = findControlMessage(InternalID, Msg, today, startTime, endTime);

		System.out.println(MSG_DMSControlRequest);	
		String a = proxy.OP_ShareDMSControl(MSG_DMSControlRequest);

		System.out.println("------------------------");
		System.out.println(a);		

	}
	
	
	private static String findControlMessage(String InternalID, String Msg, String localDate, String startTime, String endTime){
		
		String part1 = "<dMSControlRequest><organization-information><organization-id>35006001</organization-id><organization-name>Ames</organization-name><center-id>1</center-id></organization-information><device-id>"
					+ InternalID
					+ "</device-id><request-id>intrans</request-id><operator-id>intrans</operator-id><user-id>IOWASIMS\\exttis\\USER</user-id><password>IOWAt1s</password><dms-beacon-control>0</dms-beacon-control>";

		String part2 = "<dms-message>" + Msg + "</dms-message>";
		
		String part3 = "<command-request-priority>1</command-request-priority><request-date-time><local-date>"
					+ localDate + "</local-date>" 
					+ "<local-time>" + startTime + "</local-time><utc-offset>-0500</utc-offset></request-date-time>"
					+ "<command-end-time>" + endTime + "</command-end-time></dMSControlRequest>";

		return part1 + part2 + part3;
	}

}
