package online;

import java.util.HashMap;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class test {
	private final static String DETECTOR_ID_1 = "IWZ3518 - I-35 NB @ MM 118.3"; 
	private final static String DETECTOR_ID_2 = "IWZ3062 - I-35 NB @ MM 118.7"; 
	private final static String INTERNAL_ID = "310";
	
	public static void main(String[] args) throws Exception {
		// //Create HTTP Client
		HttpClient client = HttpClients.createDefault();
		//Initialize HTTP Post Request
		HttpPost post = new HttpPost("http://reactor.centralus.cloudapp.azure.com:8080/workzone/feeds/consume");
		
		String JsonStr1 = getJsonString("AMESNORTH", DETECTOR_ID_1, 50.5, "SLOW");
		String JsonStr2 = getJsonString("AMESNORTH", DETECTOR_ID_2, 60.5, "BLANK");
		
		System.out.println(JsonStr1);
		System.out.println(JsonStr2);
		System.out.println("{\"workzone\":\"6D\",\"device\":\"NIUzy\",\"avgSpeed\":32,\"alert\":40}");
	}
	
	private static String getJsonString(String workzoneName, String sensorID, double aveSpeed, String Msg){
		String jsonString = "{\"workzone\":\"" + workzoneName + "\",\"device\":\"" + 
					  sensorID + "\",\"avgSpeed\":" + aveSpeed + ",\"alert\":\"" + Msg + "\"}";
		return jsonString;
	}

}
