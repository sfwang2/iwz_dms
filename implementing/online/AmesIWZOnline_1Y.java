package online;

import java.io.File;
import java.text.SimpleDateFormat;

import utilMethods.VSLControlClass;

import com.transcore.webservices.SVC_C2CSoapProxy;

import java.io.FileWriter;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;

import jwave.Transform;
import jwave.transforms.FastWaveletTransform;
import jwave.transforms.wavelets.haar.Haar1;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * @author Shefang Wang
 * Testing the algorithm using the real streaming feeds
 */
public class AmesIWZOnline_1Y {
	private final static int UPDATE_RATE = 20 * 1000; // every 20 seconds
	private final static String DETECTOR_ID_1 = "I-35 SB to ORALABOR ROAD-SB"; 
//	private final static String DETECTOR_ID_2 = "I-35/80 EB @ MP 128.5"; 
	private final static String INTERNAL_ID = "310";
	private final static String WORKZONENAME = "Testing1Y";
	
	
	private static final double THREHOLDLEVEL_OCCU = 4;
	private static final double THREHOLDLEVEL_SPEED = 5.5;
	private static final int N = 64; // for buffersize number, must be the power of 2, 32 is about 10 min, 64 is about 20 mins
	private static final int INTERVAL = 6; // each message will be displayed at least 20*6 seconds.
	
	
	private static String FOLDERPATH = "U:/VSL_Implement/onlineControl/I-35-1Y/";
	private static String TEMPXMLPATH = "./tempXML_Ames";
	
	public static void main(String[] args) throws Exception {
		// mappers for the prediction
		HashMap<Integer, String> MsgMap = new HashMap<Integer, String>();
		MsgMap.put(40, "STOP");
		MsgMap.put(60, "SLOW");
		MsgMap.put(70, "BLANK");
		

		
		// init first
		VSLControlClass vsl1 = new VSLControlClass(DETECTOR_ID_1, INTERNAL_ID);		
		double[] speedArray1 = vsl1.getSpeedArr();
		double[]  occuArray1 = vsl1.getOccuArr();
		String[]  timeArray1 = vsl1.getTimeArr();
		
		
		//Transform 
		Transform wt = new Transform( new FastWaveletTransform( new Haar1( ) ) );
		
		// counter for burning up period
		int i_1 = 0;
	
		while(true){
			long startTime = System.currentTimeMillis();
			
			// download the temp xml file
			vsl1.downloadXML(TEMPXMLPATH);
			String xmlPath = TEMPXMLPATH + File.separator + "WavetronixTemp.xml";
			
			String info1 = vsl1.getDetectorInfo(xmlPath);

			
			//date, startTime, endTime, timeStartSec, timeEndSec, vol, occu, speed;
			String time1  = info1.split(",")[3]; //start time in seconds
			String speed1 = info1.split(",")[7]; //speed
			String occu1  = info1.split(",")[6]; //occu
			boolean initFlag_1 = (i_1 < N) && (!info1.contains("-1"));

			
			// update buffer values for sensor1
			if (initFlag_1){
				speedArray1[i_1] = Double.parseDouble(speed1);
				occuArray1[i_1]  = Double.parseDouble(occu1);
				timeArray1[i_1]  = time1;
				i_1++;				
			}
			
			
			if(i_1 >= N){
				System.out.println("Done with initialization ");
				i_1++;	
				break;
			}
			
			System.out.println("initializing-" + DETECTOR_ID_1 + " " + i_1 + ": " + info1);
					
			//wait till next 20 seconds
			wait4it(System.currentTimeMillis(), startTime);
		}
		
		//build the proxy object
		SVC_C2CSoapProxy proxy = new SVC_C2CSoapProxy();
		
		// start prediction
		int index_1 = 1;
		int predictedMsg1 = 70;
		int index_pooled = 1;
		while(true){
			try{
				long startTime = System.currentTimeMillis();
				
				String today = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());			
				String filePath = today + ".csv";
				FileWriter fw = new FileWriter(FOLDERPATH + filePath, true);
				
				String MDSfilaPath = today + "_DMS.csv";
				FileWriter fw_DMS = new FileWriter(FOLDERPATH + MDSfilaPath, true);
				
				// download the temp xml file
				vsl1.downloadXML(TEMPXMLPATH);
				String xmlPath = TEMPXMLPATH + File.separator + "WavetronixTemp.xml";
				
				String info1 = vsl1.getDetectorInfo(xmlPath);

			
				//date, startTime, endTime, timeStartSec, timeEndSec, vol, occu, speed;
				String time1  = info1.split(",")[3]; //start time in seconds
				String speed1 = info1.split(",")[7]; //speed
				String occu1  = info1.split(",")[6]; //occu
				String infoDate1 = info1.split(",")[0];
				String infoStartTime1 = info1.split(",")[1]; //start time in HHmmss
				
				
				
////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////send message to API
////////////////////////////////////////////////////////////////////////////////			
				// sensor 1
				boolean flag1 = !timeArray1[N-1].equals(time1) && // no time duplication
						!info1.contains("-1") 		   && // has something detected 
						today.equals(infoDate1);		  // today's date
				
				
				double filtered_Speed1_w = 70.0;
				double filtered_Occu1_w = 3.5;
				if(flag1){
					// update arrays
					vsl1.updataArrays(speed1, occu1, time1);
					
					//Transform and smoothing
					filtered_Speed1_w = vsl1.findFilteredValue(speedArray1, THREHOLDLEVEL_SPEED, wt);
					filtered_Occu1_w  = vsl1.findFilteredValue(occuArray1, THREHOLDLEVEL_OCCU, wt);
					
					predictedMsg1 = prediction(filtered_Speed1_w, filtered_Occu1_w);
					
					//writre the prediction info to the file
					String ans = DETECTOR_ID_1 + "," + today + "," + infoStartTime1 + "," + time1 + "," + 
							 speed1 + "," +occu1 + "," + filtered_Speed1_w + "," + filtered_Occu1_w + "," + 
							 predictedMsg1;		
					System.out.println(ans);
					fw.write(ans + "\n");					
					fw.flush();	
					
				}
				

								
				
////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////send message to DMS port
////////////////////////////////////////////////////////////////////////////////
				String msg = "BLANK";
				if(flag1){
					if( index_pooled == 1){
						msg = MsgMap.get(predictedMsg1);
						
						//write the prediction to the local DMS
						String controlStrStartTime = vsl1.addingTimeString(-5, infoStartTime1);
						String controlStrEndTime = vsl1.addingTimeString(30, infoStartTime1);
						String controlMessageString = vsl1.findControlMessage(INTERNAL_ID, 
																		msg, 
																		today, 
																		controlStrStartTime, 
																		controlStrEndTime);
						proxy.OP_ShareDMSControl(controlMessageString);	
						
						
						String DMSStr1 = DETECTOR_ID_1 + "," + today + "," + infoStartTime1 + "," + time1 + "," + 
								 speed1 + "," +occu1 + "," + filtered_Speed1_w + "," + filtered_Occu1_w + "," + 
								 predictedMsg1;	
						fw_DMS.write(DMSStr1 + "\n");
						fw_DMS.flush();	
					}
					
					
					if(index_pooled <= INTERVAL){
						index_pooled++;
						if(index_pooled == INTERVAL){
							index_pooled = 1;
						}
					}
				}
				
				
				fw_DMS.close();
				fw.close();	
				//wait for every 20 seconds.			
				wait4it(System.currentTimeMillis(), startTime);					
			} catch(Exception e){
				Thread.sleep(UPDATE_RATE);
				continue;
			}
		}
	}

	
	/**
	 * Prediction Trees
	 * @param speed
	 * @param occu
	 * @return
	 */
	private static int prediction(double speed, double occu){
		if (speed > 63.437317){
			return 70; // "None
		} else {
			if (speed <= 41.518901){
				return 40; // "Stop
			} else {
				if (occu <= 18.992188){
					return 60; // "Slow
				} else {
					return 40; // "Stop
				}
			}
		}
	}

	//wait function to wait till 20 seconds.
	private static void wait4it(long curT, long startT) throws Exception{
		long duration = curT - startT;		
		if (duration < UPDATE_RATE) {
			Thread.sleep(UPDATE_RATE - duration);
		}		
	}

	/**
	 * Json String function to send out msg to API
	 * @param workzoneName
	 * @param sensorID
	 * @param aveSpeed
	 * @param Msg
	 * @return
	 */
	private static String getJsonString(String workzoneName, String sensorID, double aveSpeed, String Msg){
		String jsonString = "{\"workzone\":\"" + workzoneName + "\",\"device\":\"" + 
					  sensorID + "\",\"avgSpeed\":" + aveSpeed + ",\"alert\":\"" + Msg + "\"}";
		return jsonString;
	}
	
}
