package utilMethods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class UtilityMethods {

	public static void downloadXML() throws Exception{
		String link = "http://205.221.97.102/Iowa.Sims.AllSites.C2C.Geofenced/"
				+ "IADOT_SIMS_AllSites_C2C.asmx/OP_ShareTrafficDetectorData?"
				+ "MSG_TrafficDetectorDataRequest=stringHTTP/1.1";
		URL url = new URL(link);
		
		File folderDir = new File("./tempXML");
		
		if(!folderDir.exists()){
			folderDir.mkdirs();
		}
		
		String fileName = "WavetronixTemp.xml";
		
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		File f = new File("./tempXML" + File.separator + fileName);
		FileWriter fw = new FileWriter(f);
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			fw.write(inputLine);
			fw.write("\n");
		}
		in.close();
		fw.close();		
	}
	
	
	
	
	/**
	 * 
	 * @param detectorID, desired detector ID
	 * @return date, startTime, endTime, timeStartSec, timeEndSec, Vol, Occu, Speed;
	 */
	public static String parseXML(String detectorID, String path) throws Exception{
//		String path = "./tempXML/WavetronixTemp.xml";
		
		Document doc = normizedDoc(path);
		
		
		//find the date of time
		NodeList dateList = doc.getElementsByTagName("local-date");
		String date = dateList.item(0).getTextContent();
		
		
		//find the start-time
		NodeList startList = doc.getElementsByTagName("start-time");		
		String startTime = startList.item(0).getTextContent();
		
		//find the end-time
		NodeList endList = doc.getElementsByTagName("end-time");		
		String endTime = endList.item(0).getTextContent();

		int timeStartSec = convertToSec(startTime);
		int timeEndSec = convertToSec(endTime);
		
		
		//find the desired node using XPath
		XPath xpath = XPathFactory.newInstance().newXPath();
		String expString = "//detector-report[detector-id = \"" + detectorID + "\"]";	
		
		String trafficInfo = "";
		// Sometimes, the data does not have any sensor Info(can not find sensor Node), so just assign "-1,-1,-1"
		try{
			Node node = (Node) xpath.compile(expString).evaluate(doc, XPathConstants.NODE);
			trafficInfo = findTrafficInfo(node);
		} catch (Exception e){
			trafficInfo = "-1,-1,-1";	
		}
		
		String ans = date + "," + startTime + "," + endTime + "," + timeStartSec + "," + timeEndSec + "," + trafficInfo;		
		return ans;
	}
	
	
	//Normal procedura to setup parsing xml files. return a root Dom tree Document obj
	public static Document normizedDoc(String path) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(path);
		
		doc.getDocumentElement().normalize();
		return doc;
	}
	
	
	
	//find&average the traffic information for each detector vol/occu/speed
	//if the detector is not "operational" or no cars passing by, return "null"
	public static String findTrafficInfo(Node detectorNode){
		
//		NodeList statusNode = ((Element)detectorNode).getElementsByTagName("status");		
//		String status = statusNode.item(0).getTextContent();
		
		
		NodeList lanes = ((Element)detectorNode).getElementsByTagName("lane");
		int numLanes = lanes.getLength();
		
		int vol = 0;
		double occu = 0;
		double speed = 0;
		
		
		for (int i = 0; i < numLanes; i++){
			Element lane = (Element)(lanes.item(i));
			
			
			//some data may not even contains these tag, which can raise the exception, if so, continue.
			try{
				lane.getElementsByTagName("volume").item(0).getTextContent().trim();
				lane.getElementsByTagName("occupancy").item(0).getTextContent().trim();
				lane.getElementsByTagName("speed").item(0).getTextContent().trim();
			} catch(Exception e){
				continue;
			}
			
			
			String laneVol = lane.getElementsByTagName("volume").item(0).getTextContent().trim();
			String laneOccu = lane.getElementsByTagName("occupancy").item(0).getTextContent().trim();
			String laneSpeed = lane.getElementsByTagName("speed").item(0).getTextContent().trim();
			
			if(laneVol.length()>0 && laneOccu.length()>0 && laneSpeed.length()> 0) {
				vol = vol + Integer.parseInt(laneVol);
				occu = occu + Double.parseDouble(laneOccu);
				
				//speed is weighted by the volume
				speed = speed + Double.parseDouble(laneSpeed) * Integer.parseInt(laneVol);				
			}			
		}
		
		
		String info = "";
//		 && status.equals("operational")
		if(vol > 0 && numLanes > 0){			
			info = vol + "," + occu/numLanes + "," + speed/vol*0.621371;
		} else {
			info = "-1,-1,-1";			
		}
				
		return info;
	}
	
	
	/**
	 * Convert time string to desired format
	 * 224740 -> 22*3600 + 47*60 + 40
	 */	
	private static int convertToSec(String str){
		String hh = str.substring(0,2);
		String mm = str.substring(2,4);
		String ss = str.substring(4);
		
		int strInSec = Integer.parseInt(hh) * 3600 + Integer.parseInt(mm) * 60 + Integer.parseInt(ss);
		return strInSec;		
	}
	
}
