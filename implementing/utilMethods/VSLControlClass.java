package utilMethods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jwave.Transform;
import jwave.transforms.FastWaveletTransform;
import jwave.transforms.wavelets.haar.Haar1;


import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.Utils;


public class VSLControlClass {
	
	private int UPDATE_RATE = 20 * 1000; // every 20 seconds
	private String DETECTOR_ID = "I-35 SB @ MP 94"; 
	private int N = 64; // for FFT number, must be the power of 2, 32 is about 10 min, 64 is about 20 mins
	private double THREHOLDLEVEL_OCCU = 5;
	private double THREHOLDLEVEL_SPEED = 3;
	private static String MODELPATH = "U:/VSL_Implement/Logistic.model";
	private static String FOLDERPATH = "U:/VSL_Implement/";
	
	
	private double[] speedArray;
	private double[]  occuArray;
	private String[]  timeArray;
	private String detectorID;
	private String internalID;
	
	
	public VSLControlClass(String sensorID, String transCoreID){
		detectorID = sensorID;
		internalID = transCoreID;
		speedArray = new double[N];
		occuArray  = new double[N];
		timeArray  = new String[N];
	}

	
	
	public String[] getTimeArr(){
		return timeArray;
	}
	
	public double[] getSpeedArr(){
		return speedArray;
	}
	
	public double[] getOccuArr(){
		return occuArray;
	}
	
	
	
	public void fillArrays(int inputIndex, String speed, String occu, String time) {	
		speedArray[inputIndex] = Double.parseDouble(speed);
		occuArray[inputIndex]  = Double.parseDouble(occu);
		timeArray[inputIndex]  = time;
	}
	
	
	public void updataArrays(String speed, String occu, String time){
		for (int j = 0; j < N - 1; j++) {
			speedArray[j] = speedArray[j + 1];
			 occuArray[j] =  occuArray[j + 1];
			 timeArray[j] =  timeArray[j + 1];		
		}
		speedArray[N - 1] = Double.parseDouble(speed);
		 occuArray[N - 1] = Double.parseDouble(occu);
		 timeArray[N - 1] = time;
	}
	
	
	
	public void downloadXML(String path) throws Exception{
		String link = "http://205.221.97.102/Iowa.Sims.AllSites.C2C.Geofenced/"
				+ "IADOT_SIMS_AllSites_C2C.asmx/OP_ShareTrafficDetectorData?"
				+ "MSG_TrafficDetectorDataRequest=stringHTTP/1.1";
		URL url = new URL(link);
		
//		File folderDir = new File("./tempXML");
		File folderDir = new File(path);
		
		if(!folderDir.exists()){
			folderDir.mkdirs();
		}
		
		String fileName = "WavetronixTemp.xml";
		
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		File f = new File(path + File.separator + fileName);
		FileWriter fw = new FileWriter(f);
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			fw.write(inputLine);
			fw.write("\n");
		}
		in.close();
		fw.close();		
	}
	
	
	
	public String getDetectorInfo(String path) throws Exception{
		String info = UtilityMethods.parseXML(detectorID, path);
		return info;
	}
	
	
	/**
	 * filtered Transformed arrays
	 * @param arr 
	 * @param level, of threshold
	 * @param t, transform method
	 * @return
	 */
	public double findFilteredValue(double[] arr, double level , Transform t){
		
		double threshold = findThreshold(arr, t) * level;
		double[] arrTransformed = t.forward(arr);
				
		for (int i = 0; i < arr.length; i++) {
			boolean flag = Math.abs(arrTransformed[i]) <=  threshold;
			if(flag){
				arrTransformed[i] = 0;
			}
		}		
		
		//just return the last item of the filtered value
		double[] filtered_Transform_reverse = t.reverse(arrTransformed);
		
		int returnInted = arr.length - 1;
		double ans = filtered_Transform_reverse[returnInted];
		if(ans >= 0){
			return ans;
		} else {
			return 0;
		}
		
	}

	
		
	/**
	 * find the Transform threshold. 
	 * @param arr
	 * @return
	 */
	private static double findThreshold(double[] arr, Transform t){
		//Transform t = new Transform( new FastWaveletTransform( new Daubechies10( ) ) );
		double[] arrTransformed = t.forward(arr);
		double sum = 0;
		for (int i = 1; i < arr.length - 1; i++) {
			double temp = Math.pow(0.5* ( Math.abs(arrTransformed[i-1]) -   Math.abs(arrTransformed[i]) +  0.5 * ( Math.abs(arrTransformed[i+1]))), 2);
			sum = sum + temp;
		}		
		return Math.sqrt(2 * sum / (3*(arr.length - 2)));
	}

	
	
	
	
	/**
	 * @param InternalID 	-> 310(NE158), 317(NE126), 309(MP94), 389(1stAve Ankeny)
	 * @param Msg 			-> just simple speed limit number
	 * @param localDate 	-> 20161014
	 * @param startTime 	-> 142000
	 * @param endTime		-> 144000
	 * @return				-> control XML string
	 */
	public String findControlMessage(String InternalID, String Msg, String localDate, String startTime, String endTime){
		
		String part1 = "<dMSControlRequest><organization-information><organization-id>35006001</organization-id><organization-name>Ames</organization-name><center-id>1</center-id></organization-information><device-id>"
					+ InternalID
					+ "</device-id><request-id>intrans</request-id><operator-id>intrans</operator-id><user-id>IOWASIMS\\exttis\\USER</user-id><password>IOWAt1s</password><dms-beacon-control>0</dms-beacon-control>";

		String part2 = "<dms-message>" + Msg + "</dms-message>";
		
		String part3 = "<command-request-priority>1</command-request-priority><request-date-time><local-date>"
					+ localDate + "</local-date>" 
					+ "<local-time>" + startTime + "</local-time><utc-offset>-0500</utc-offset></request-date-time>"
					+ "<command-end-time>" + endTime + "</command-end-time></dMSControlRequest>";

		return part1 + part2 + part3;
	}
	
	
	
	/**
	*  Convenience method to add a specified number of minutes to a Date object
	*  From: http://stackoverflow.com/questions/9043981/how-to-add-minutes-to-my-date
	*  @param  minutes  The number of minutes to add
	*  @param  beforeTime  The time that will have minutes added to it
	*  @return  A date object(in string) with the specified number of minutes added to it 
	*/	
	public String addingTimeString(int addingMinutes, String currentTimeString) throws ParseException{
		final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs
		
		SimpleDateFormat df = new SimpleDateFormat("HHmmss");
		Date currentTime = df.parse(currentTimeString);
		long curTimeInMs = currentTime.getTime();
		Date afterAddingMins = new Date(curTimeInMs + (addingMinutes * ONE_MINUTE_IN_MILLIS));		
		
		return df.format(afterAddingMins);
	}
	
	
	
	
	
	public Instances buildDataSet(){
		Attribute speed = new Attribute("speed");
		Attribute occu  = new Attribute("occu");
		
		ArrayList<String> labels = new ArrayList<>();
		labels.add("70");
		labels.add("55");
		labels.add("45");
		labels.add("35");
		Attribute vsl = new Attribute("label", labels);
		
		ArrayList<Attribute> attrs = new ArrayList<>();
		attrs.add(speed);
		attrs.add(occu);
		attrs.add(vsl);
		Instances datasets = new Instances("prediction", attrs, 1);
				
		return datasets;
	}
	
}
